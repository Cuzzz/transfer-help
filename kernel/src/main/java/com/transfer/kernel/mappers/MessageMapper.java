package com.transfer.kernel.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.transfer.kernel.dao.MessageRepository;
import com.transfer.kernel.dao.PersonRepository;
import com.transfer.kernel.domains.Message;
import com.transfer.kernel.dto.MessageDto;

@Component
@Scope("singleton")
public class MessageMapper {

	@Autowired
	PersonRepository personRepo;
	
	@Autowired
	MessageRepository msgRepo;
	
	public Message getMessage(MessageDto msgDto){
		Message msg = new Message();
		msg.setId(msgDto.getId());
		msg.setTitle(msgDto.getTitle());
		msg.setBody(msgDto.getBody());
		msg.setStatus(msgDto.getStatus());
		msg.setUpperMessage(msgRepo.findOne(msgDto.getUpperMessageId()));
		msg.setAuthor(personRepo.findOne(msgDto.getAuthorId()));
		msg.setDestination(personRepo.findOne(msgDto.getDestinationId()));
		return msg;
	}
	
	public MessageDto getMessageDto(Message msg){
		MessageDto msgDto = new MessageDto();
		msgDto.setId(msg.getId());
		msgDto.setTitle(msg.getTitle());
		msgDto.setBody(msg.getBody());
		msgDto.setStatus(msg.getStatus());
		if(msg.getUpperMessage() != null)
			msgDto.setUpperMessageId(msg.getUpperMessage().getId());
		msgDto.setAuthorId(msg.getAuthor().getId());
		msgDto.setDestinationId(msg.getDestination().getId());
		return msgDto;
	}
}
