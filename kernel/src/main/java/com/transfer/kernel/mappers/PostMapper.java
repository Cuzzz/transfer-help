package com.transfer.kernel.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.transfer.kernel.domains.Post;
import com.transfer.kernel.dto.PostDto;
import com.transfer.kernel.service.PersonService;

@Component
public class PostMapper {

	@Autowired
	PersonService personService;
	
	public Post getPost(PostDto postDto){
		Post post = new Post();
		post.setId(postDto.getId());
		post.setArrival(postDto.getArrival());
		post.setBody(postDto.getBody());
		post.setDeparture(postDto.getDeparture());
		post.setFd(postDto.getFd());
		post.setPerson(personService.get(postDto.getPersonId()));
		post.setRoute(postDto.getRoute());
		post.setType(postDto.getType());
		return post;
	}
	
	public PostDto getPostDto(Post post){
		PostDto postDto = new PostDto();
		postDto.setArrival(post.getArrival());
		postDto.setBody(post.getBody());
		postDto.setDeparture(post.getDeparture());
		postDto.setFd(post.getFd());
		postDto.setId(post.getId());
		postDto.setPersonId(post.getPerson().getId());
		postDto.setRoute(post.getRoute());
		postDto.setType(post.getType());
		return postDto;
	}
	
	
}
