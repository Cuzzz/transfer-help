package com.transfer.kernel.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class PersonDto {

	private long id;
	private Timestamp fd;
	private Timestamp td;
	private String firstName;
	private String surname;
	private Date birthDate;
	private String phoneNumber;
	private String avatarUrl;
	private String about;
	private float generalRating;
	private String status;
	private long userId;
	private String gender;
	private long cityId;
	private int counter;
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getId() {
		return id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFd() {
		return fd;
	}

	public void setFd(Timestamp fd) {
		this.fd = fd;
	}

	public Timestamp getTd() {
		return td;
	}

	public void setTd(Timestamp td) {
		this.td = td;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public float getGeneralRating() {
		return generalRating;
	}

	public void setGeneralRating(float generalRating) {
		this.generalRating = generalRating;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}