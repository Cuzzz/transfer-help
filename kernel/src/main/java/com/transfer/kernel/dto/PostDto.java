package com.transfer.kernel.dto;

import java.sql.Timestamp;

public class PostDto {

	private long id;
	private String body;
	private String route;
	private byte type;
	private Timestamp fd;
	private long personId;
	private Timestamp departure;
	private Timestamp arrival;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public byte getType() {
		return type;
	}
	public void setType(byte type) {
		this.type = type;
	}
	public Timestamp getFd() {
		return fd;
	}
	public void setFd(Timestamp fd) {
		this.fd = fd;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public Timestamp getDeparture() {
		return departure;
	}
	public void setDeparture(Timestamp departure) {
		this.departure = departure;
	}
	public Timestamp getArrival() {
		return arrival;
	}
	public void setArrival(Timestamp arrival) {
		this.arrival = arrival;
	}
}
