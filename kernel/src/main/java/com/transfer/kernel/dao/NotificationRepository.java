package com.transfer.kernel.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfer.kernel.domains.Notification;
import com.transfer.kernel.domains.Person;

public interface NotificationRepository extends JpaRepository<Notification, Long>{

	List<Notification> findByDestinationAndStatus(Person person, int status);
	
}
