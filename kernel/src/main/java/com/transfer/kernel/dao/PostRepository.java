package com.transfer.kernel.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.transfer.kernel.domains.Person;
import com.transfer.kernel.domains.Post;

public interface PostRepository extends JpaRepository<Post, Long>{

	public List<Post> findByPerson(Person person);
	
	public Page<Post> findByType(byte type, Pageable pageable);

	public List<Post> findByType(byte type, Sort sort);
}
