package com.transfer.kernel.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfer.kernel.domains.City;

public interface CityRepository extends JpaRepository<City, Long> {

}
