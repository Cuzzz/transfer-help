package com.transfer.kernel.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfer.kernel.domains.Person;
import com.transfer.kernel.security.User;

public interface PersonRepository extends JpaRepository<Person, Long>{

	public Person findByUser(User user);
	public Person findByUserId(long id);
	
}
