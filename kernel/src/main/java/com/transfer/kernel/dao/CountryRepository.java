package com.transfer.kernel.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfer.kernel.domains.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

}
