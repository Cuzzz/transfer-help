package com.transfer.kernel.emuns;

public enum PostType {
	
	NEED((byte) 1),
	CAN_HELP((byte) 2);
	
	private byte code;
	
	PostType(byte code) {
		this.code = code;
	}
	
	public byte getCode(){
		return code;
	}
	
}
