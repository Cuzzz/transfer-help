package com.transfer.kernel.emuns;

public enum MessageStatus {

	NEW(1),
	READ(2);
	
	private int code;
	
	MessageStatus(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
}
