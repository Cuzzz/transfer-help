package com.transfer.kernel.emuns;

public enum NotificationStatus {

	NEW (1),
	READ(2);
	
	private int code;
	
	NotificationStatus(int code) {
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
}
