package com.transfer.kernel.service;

import java.util.List;

import com.transfer.kernel.domains.Person;
import com.transfer.kernel.security.User;

public interface PersonService {

	void save(Person person);
	void update(Person person) throws Exception;
	void delete(Person person);
	Person get(long id);
	Person getByUserId(long id);
	Person getByUser(User user);
	List<Person> findAll();
	
}
