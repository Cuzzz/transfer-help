package com.transfer.kernel.service;

import java.security.MessageDigest;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.transfer.kernel.dao.PersonRepository;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.security.User;

@Service("personService")
public class PersonServiceImpl implements PersonService{

	@Autowired
	PersonRepository personRepo;
	
	@Autowired
	Environment env;
	
	public void save(Person person) {
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest((person.getUser().getEmail()+person.getUser().getUsername()).getBytes("UTF-8"));
			person.getUser().setActive(hash.toString());
			//person.getUser().setActive("active");
			personRepo.save(person);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void update(Person person) throws Exception {
		Person savedPerson = personRepo.findOne(person.getId());
		if(savedPerson == null)
			throw new Exception("Cannot update person with id="+person.getId()+" , not found");
		person.setUser(savedPerson.getUser());
		personRepo.saveAndFlush(person);
	}

	public void delete(Person person) {
		personRepo.delete(person);
	}

	public Person get(long id) {
		return personRepo.getOne(id);
	}
		
	public Person getByUserId(long id) {
		// TODO Auto-generated method stub
		return personRepo.findByUserId(id);
	}

	public Person getByUser(User user) {
		return personRepo.findByUser(user);
	}

	public List<Person> findAll() {
		return personRepo.findAll();
	}
}
