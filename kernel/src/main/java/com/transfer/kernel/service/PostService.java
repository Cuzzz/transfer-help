package com.transfer.kernel.service;

import java.util.List;

import com.transfer.kernel.domains.Person;
import com.transfer.kernel.domains.Post;

public interface PostService {

	public Post save(Post post);
	public Post update(Post post) throws Exception;
	public void delete(Post post);
	public void delete(long id);
	Post get(long id);
	public List<Post> getAuthorList(Person person);
	public List<Post> getPaginatedList(int page, int count, byte type);
}
