package com.transfer.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transfer.kernel.dao.NotificationRepository;
import com.transfer.kernel.domains.Notification;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.emuns.NotificationStatus;

@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	NotificationRepository noteRepo;
	
	public void save(Notification note) {
		noteRepo.save(note);
	}

	public void read(long noteId) throws Exception {
		Notification savedNote = noteRepo.findOne(noteId);
		if(savedNote == null)
			throw new Exception("Notification not found, id = " + noteId );
		savedNote.setStatus(NotificationStatus.READ.getCode());
		noteRepo.saveAndFlush(savedNote);
	}

	@Override
	public List<Notification> getByPerson(Person person) {
		List<Notification> list = noteRepo.findByDestinationAndStatus(person
											, NotificationStatus.NEW.getCode());
		return list;
	}

}
