package com.transfer.kernel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.transfer.kernel.dao.PostRepository;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.domains.Post;

@Service("postService")
public class PostServiceImpl implements PostService{

	@Autowired
	PostRepository postRepo;
	
	@Autowired 
	PersonService personService;
	
	private Sort sort = new Sort(new Order(Sort.Direction.DESC, "id"));
	
	public Post save(Post post) {
		Post result = postRepo.save(post);
		return result;
	}

	public Post update(Post post) throws Exception {
		Post savedPost = postRepo.findOne(post.getId());
		if(savedPost == null)
			throw new Exception("Job not found id="+post.getId());
		post.setPerson(savedPost.getPerson());
		Post result = postRepo.save(post);
		return result;
	}

	public void delete(Post post) {
		postRepo.delete(post);
	}

	public void delete(long id) {
		postRepo.delete(id);
	}

	public Post get(long id) {
		Post post = postRepo.findOne(id);
		return post;
	}

	public List<Post> getAuthorList(Person person) {
		List<Post> result = postRepo.findByPerson(person);
		return result;
	}

	public List<Post> getPaginatedList(int page, int count, byte type) {
		Pageable limit = new PageRequest(page, count, Sort.Direction.DESC, "id");
		Page<Post> result = postRepo.findByType(type, limit);
		return result.getContent();
	}

}
