package com.transfer.kernel.service;

import java.util.List;

import com.transfer.kernel.domains.Message;
import com.transfer.kernel.domains.Person;

public interface MessageService {

	long save(Message msg);
	long read(Message msg) throws Exception;
	long deleteForDestination(Message msg) throws Exception;
	long deleteForAuthor(Message msg) throws Exception;
	List<Message> getByAuthor(Person author);
	List<Message> getByDestination(Person dest);
	Message findOne(long id);
	
}
