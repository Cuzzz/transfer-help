package com.transfer.kernel.service;

import java.util.List;

import com.transfer.kernel.domains.Notification;
import com.transfer.kernel.domains.Person;

public interface NotificationService {

	void save(Notification note);
	void read(long noteId) throws Exception;
	List<Notification> getByPerson(Person person);
	
}
