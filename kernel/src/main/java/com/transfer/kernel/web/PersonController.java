package com.transfer.kernel.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.transfer.kernel.commons.CityInit;
import com.transfer.kernel.commons.JsonResponse;
import com.transfer.kernel.commons.MailSender;
import com.transfer.kernel.dao.CityRepository;
import com.transfer.kernel.dao.CountryRepository;
import com.transfer.kernel.domains.City;
import com.transfer.kernel.domains.Country;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.dto.PersonDto;
import com.transfer.kernel.mappers.PersonMapper;
import com.transfer.kernel.security.User;
import com.transfer.kernel.security.UserAuthentication;
import com.transfer.kernel.security.UserRepository;
import com.transfer.kernel.security.UserRole;
import com.transfer.kernel.service.PersonService;

/**
 * Example controller for checking right configuration of hibernate for checking
 * saving/getting users just request to localhost:8080/save better if you repeat
 * this by some times and then you can call? for example localhost:8080/find/2
 * and get json response with person info
 */
@RestController
public class PersonController {

	@Autowired
	PersonService personService;

	@Autowired
	UserRepository userRepo;

	@Autowired
	PersonMapper personMapper;

	@Autowired
	CityRepository cityRepo;

	@Autowired
	ApplicationContext ctx;

	private final String REG_MSG = "You have registered at craftbe.org. For activating your "
			+ "account please follow <a href=\"http://transfer-areznikov.rhcloud.com/person/activate/%s\">link</a>";

	@RequestMapping(value = "/person/register", method = RequestMethod.POST)
	public JsonResponse register(@RequestBody Person person) {
		try {
			User user = person.getUser();
			user.setPassword(new BCryptPasswordEncoder().encode(user
					.getPassword()));
			user.grantRole(UserRole.USER);
			userRepo.save(user);
			personService.save(person);
			MailSender.sendMail("Registation at transfer-help.org",
					String.format(REG_MSG, user.getActive()), user.getEmail());
			return new JsonResponse(true);
		} catch (Exception e) {
			return new JsonResponse(false, e.getMessage());
		}
	}

	@RequestMapping(value = "/person/find/{id}", method = RequestMethod.GET)
	public JsonResponse getPersonInfo(@PathVariable long id) {
		// Person persons = personService.get(id);
		Person person = personService.getByUserId(id);
		if (person == null)
			return new JsonResponse(false, "person not found");
		PersonDto personDto = personMapper.getPersonDto(person);
		return new JsonResponse(true).setBody(personDto);
	}

	@RequestMapping(value = "/person/update", method = RequestMethod.PATCH)
	public JsonResponse updatePersonInfo(@RequestBody PersonDto personDto) {
		try {
			Person savedPerson = personService.get(personDto.getId());
			System.out.println(savedPerson.getId());
			System.out.println(getCurrentUser().getId());
			if (!savedPerson.equals(getCurrentUser()))
				throw new Exception("Not owner trying to update account");
			Person person = personMapper.getPerson(personDto);
			personService.update(person);
			return new JsonResponse(true);
		} catch (Exception e) {
			return new JsonResponse(false, e.getMessage());
		}
	}

	@RequestMapping(value = "/person/activate/{code}", method = RequestMethod.GET)
	public JsonResponse activatePerson(@PathVariable String code) {
		User user = userRepo.findByActive(code);
		if (user == null) {
			return new JsonResponse(false, "User not found or already active");
		}
		user.setActive("active");
		userRepo.save(user);
		return new JsonResponse(true);
	}

	@RequestMapping(value = "/person/check", method = RequestMethod.GET)
	public JsonResponse checkByEmail(@RequestParam("email") String email) {
		User user = userRepo.findByEmail(email);
		if (user == null) {
			return new JsonResponse(true, "Email is not busy");
		}
		return new JsonResponse(false, "Email is busy try other");

	}

	@RequestMapping(value = "/city/check", method = RequestMethod.GET)
	public void checkCities() {
		System.out.println("Start checking locations");
		CountryRepository countryRepo = ctx.getBean(CountryRepository.class);
		if (countryRepo.count() == 0)
			countryRepo.save(CityInit.initCountries());
		Country russia = countryRepo.findOne(1L);
		CityRepository cityRepo = ctx.getBean(CityRepository.class);
		if (cityRepo.count() == 0)
			cityRepo.save(CityInit.initCities(russia));
		System.out.println("Finish checking locations");

	}

	private Person getCurrentUser() {
		final Authentication authentication = SecurityContextHolder
				.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}

}
