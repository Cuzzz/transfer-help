package com.transfer.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transfer.kernel.commons.JsonResponse;
import com.transfer.kernel.domains.Message;
import com.transfer.kernel.domains.Notification;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.dto.MessageDto;
import com.transfer.kernel.emuns.MessageStatus;
import com.transfer.kernel.emuns.NotificationStatus;
import com.transfer.kernel.emuns.NotificationType;
import com.transfer.kernel.mappers.MessageMapper;
import com.transfer.kernel.security.User;
import com.transfer.kernel.security.UserAuthentication;
import com.transfer.kernel.service.MessageService;
import com.transfer.kernel.service.NotificationService;
import com.transfer.kernel.service.PersonService;

@RestController
@RequestMapping("/msg")
public class MessageController {

	@Autowired
	MessageService msgService;
	
	@Autowired
	PersonService personService;
	
	@Autowired
	NotificationService noteService;
	
	@Autowired
	MessageMapper msgMapper;
	
	@RequestMapping(value = "/send", method = RequestMethod.PUT)
	public JsonResponse send(@RequestBody MessageDto msgDto){
		try{
			Message msg = msgMapper.getMessage(msgDto);
			msg.setAuthor(getCurrentUser());
			msg.setStatus(MessageStatus.NEW.getCode());
			long id = msgService.save(msg);
			Notification note = new Notification();
			note.setBody("msg:"+id);
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.MSG.getCode());
			note.setDestination(msg.getDestination());
			noteService.save(note);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/in", method = RequestMethod.GET)
	public JsonResponse getIncoming(){
		try{
			Person current = getCurrentUser();
			List<Message> list = msgService.getByDestination(current);
			List<MessageDto> resultList = new ArrayList<MessageDto>(list.size());
			for(Message msg : list){
				resultList.add(msgMapper.getMessageDto(msg));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/out", method = RequestMethod.GET)
	public JsonResponse getOutcoming(){
		try{
			Person current = getCurrentUser();
			List<Message> list = msgService.getByAuthor(current);
			List<MessageDto> resultList = new ArrayList<MessageDto>(list.size());
			for(Message msg : list){
				resultList.add(msgMapper.getMessageDto(msg));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/author/del/{id}", method = RequestMethod.PATCH)
	public JsonResponse deleteForAuthor(@PathVariable long id){
		try{
			Message msg = msgService.findOne(id);
			if(msg == null)
				throw new Exception("Msg not found, id="+id);
			Person current = getCurrentUser();
			if(!current.equals(msg.getAuthor()))
				throw new Exception("Trying delete third person msg, id = "+id);
			id = msgService.deleteForAuthor(msg);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/dest/del/{id}", method = RequestMethod.PATCH)
	public JsonResponse deleteForDestination(@PathVariable long id){
		try{
			Message msg = msgService.findOne(id);
			if(msg == null)
				throw new Exception("Msg not found, id="+id);
			Person current = getCurrentUser();
			if(!current.equals(msg.getDestination()))
				throw new Exception("Trying delete third person msg, id = "+id);
			id = msgService.deleteForDestination(msg);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read/{id}", method = RequestMethod.GET)
	public JsonResponse readMsg(@PathVariable long id){
		try{
			Message msg = msgService.findOne(id);
			if(msg == null)
				throw new Exception("Msg not found, id="+id);
			Person current = getCurrentUser();
			if(!current.equals(msg.getDestination()))
				throw new Exception("Trying read third person msg, id = "+id);
			id = msgService.read(msg);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/reply", method = RequestMethod.PUT)
	public JsonResponse reply(@RequestBody MessageDto msgDto){
		try{
			Message msg = msgMapper.getMessage(msgDto);
			msg.setAuthor(getCurrentUser());
			msg.setStatus(MessageStatus.NEW.getCode());
			long id = msgService.save(msg);
			Notification note = new Notification();
			note.setBody("msg:"+id);
			note.setStatus(NotificationStatus.NEW.getCode());
			note.setType(NotificationType.MSG.getCode());
			note.setDestination(msg.getDestination());
			noteService.save(note);
			return new JsonResponse(true).setBody(id);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public JsonResponse getSingle(@PathVariable long id){
		try{
			Message msg = msgService.findOne(id);
			if(msg == null)
				throw new Exception("No msg found, id = "+id);
			Person current = getCurrentUser();
			if(!(msg.getAuthor().equals(current) || msg.getDestination().equals(current)))
				throw new Exception("Trying to read third person msg");
			return new JsonResponse(true).setBody(msgMapper.getMessageDto(msg));
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
}
