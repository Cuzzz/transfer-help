package com.transfer.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.transfer.kernel.commons.JsonResponse;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.domains.Post;
import com.transfer.kernel.dto.PostDto;
import com.transfer.kernel.mappers.PostMapper;
import com.transfer.kernel.security.User;
import com.transfer.kernel.security.UserAuthentication;
import com.transfer.kernel.service.PersonService;
import com.transfer.kernel.service.PostService;

@RestController
@RequestMapping("/post")
public class PostController {
	
	@Autowired
	PostMapper postMapper;
	
	@Autowired
	PostService postService;
	
	@Autowired
	PersonService personService;
	
	@RequestMapping(value="/new", method = RequestMethod.PUT)
	public JsonResponse createJob(@RequestBody PostDto postDto){
		try{
			Person current = getCurrentUser();
			Post post = postMapper.getPost(postDto);
			post.setPerson(current);
			postService.save(post);
			return new JsonResponse(true).setBody(post);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/update", method = RequestMethod.PATCH)
	public JsonResponse updatePost(@RequestBody PostDto postDto){
		try{
			Post savedPost = postService.get(postDto.getId());
			if(!savedPost.getPerson().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			Post post = postMapper.getPost(postDto);
			Post result = postService.update(post);
			return new JsonResponse(true).setBody(result);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.DELETE)
	public JsonResponse deletePost(@PathVariable long id){
		try{
			Post savedPost = postService.get(id);
			if(!savedPost.getPerson().equals(getCurrentUser()))
				throw new Exception("Detected trying of updating job not by author.");
			postService.delete(id);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/get/{id}", method = RequestMethod.GET)
	public JsonResponse getPost(@PathVariable long id){
		try{
			Post post = postService.get(id);
			PostDto dto = postMapper.getPostDto(post);
			return new JsonResponse(true).setBody(dto);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value="/author", method = RequestMethod.GET)
	public JsonResponse getPostListOfAuthor(){
		try{
			Person current = getCurrentUser();
			List<Post> list = postService.getAuthorList(current);
			if(list.size() == 0)
				throw new Exception("No jobs found for authorId = " + current.getId());
			List<PostDto> resultList = new ArrayList<PostDto>(list.size());
			for(Post post : list){
				resultList.add(postMapper.getPostDto(post));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public JsonResponse getPostList(@RequestParam(required=false, defaultValue = "0") int page
								 , @RequestParam(required=false, defaultValue = "0") int count
								 , @RequestParam(required= false, defaultValue = "1") byte type){
		try{
			List<Post> list = new ArrayList<Post>();
			list = postService.getPaginatedList(page, count, type);
			List<PostDto> resultList = new ArrayList<PostDto>(list.size()); 
			for(Post post : list){
				resultList.add(postMapper.getPostDto(post));
			}
			return new JsonResponse(true).setBody(resultList);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
}
