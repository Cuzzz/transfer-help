package com.transfer.kernel.web;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transfer.kernel.domains.Person;
import com.transfer.kernel.security.UserRepository;
import com.transfer.kernel.service.PersonService;
import com.yandex.money.api.YandexMoney;
import com.yandex.money.api.methods.OperationDetails;
import com.yandex.money.api.methods.OperationDetails.Request;
import com.yandex.money.api.net.ApiClient;
import com.yandex.money.api.net.DefaultApiClient;

@RestController
public class MoneyController {

	@Autowired
	PersonService personService;
	
	@Autowired
	UserRepository userRepo;
	
	@RequestMapping(value = "/money", method = RequestMethod.POST, headers = {"content-type=application/x-www-form-urlencoded"})
	public void money(@RequestBody MultiValueMap<String,String> body) throws Exception{
		System.out.println("OPERATION: "+body);
		System.out.println("----------------------------------");
		ApiClient apiClient = new DefaultApiClient("7EBEC2A8650A442E7E2088377D637236B082472E12D6FD96315B1E8C86B37ECA");
		YandexMoney yaMoney = new YandexMoney(apiClient);
		yaMoney.setAccessToken("41001849565447.6427541863D8C072291CE20C4A4275035B89F1F2906AB931C7FD2391BA288AC4FE6B38EB8DFBA30A933A12E1607AE308B076D0AD4B0182B2679FB76430A0D2C7E11132D25568E35792B3786B5E9039234B2950BC6BCC17C8E3602706A53D830AE9F9621737A982C61BCDCA4157D85112A56A2BC3D86A24C9676EBFB781FDD716");
		OperationDetails.Request req = new Request(body.getFirst("operation_id"));
		OperationDetails details = yaMoney.execute(req);
		Person donater = personService.getByUser(userRepo.findByUsername(details.getOperation().getMessage()));
		//donater.setBalance(new BigDecimal(body.getFirst("withdraw_amount")).add(donater.getBalance()));
		personService.update(donater);
	}
	
}
