package com.transfer.kernel.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transfer.kernel.commons.JsonResponse;
import com.transfer.kernel.domains.Notification;
import com.transfer.kernel.domains.Person;
import com.transfer.kernel.dto.NotificationDto;
import com.transfer.kernel.mappers.NotificationMapper;
import com.transfer.kernel.security.User;
import com.transfer.kernel.security.UserAuthentication;
import com.transfer.kernel.service.NotificationService;
import com.transfer.kernel.service.PersonService;

@RestController
@RequestMapping("/note")
public class NotificationController {

	@Autowired
	PersonService personService;
	
	@Autowired
	NotificationService noteService;
	
	@Autowired
	NotificationMapper noteMapper;
	
	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public JsonResponse getNotesForUser(){
		try{
			Person person = getCurrentUser();
			List<Notification> list = noteService.getByPerson(person);
			List<NotificationDto> resultList = new ArrayList<NotificationDto>(list.size());
			for(Notification note : list){
				resultList.add(noteMapper.getNotificationDto(note));
			}
			JsonResponse response = new JsonResponse(true).setBody(resultList);
			return response;
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());
		}
	}
	
	@RequestMapping(value = "/read/{id}", method = RequestMethod.POST)
	public JsonResponse readNote(@PathVariable long id){
		try{
			noteService.read(id);
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());	
		}
		
	}
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public JsonResponse readAllNotesForUser(){
		try{
			Person person = getCurrentUser();
			List<Notification> list = noteService.getByPerson(person);
			for(Notification note : list){
				noteService.read(note.getId());
			}
			return new JsonResponse(true);
		}
		catch(Exception e){
			return new JsonResponse(false, e.getMessage());	
		}
	}
	
	private Person getCurrentUser(){
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (authentication instanceof UserAuthentication) {
			 user = ((UserAuthentication) authentication).getDetails();
		}
		return personService.getByUser(user);
	}
}
