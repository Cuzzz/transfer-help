package com.transfer.kernel.commons;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.transfer.kernel.domains.City;
import com.transfer.kernel.domains.Country;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CityInit {

	public static List<Country> initCountries(){
		String countryJson = getJson("http://api.vk.com/method/database.getCountries?v=5.5&need_all=1&count=1000");
		JSONObject jsonObj = new JSONObject(countryJson);
		JSONArray array = jsonObj.getJSONObject("response").getJSONArray("items"); 
		Gson gson = new Gson();
		Country country;
		List<Country> countryList = new ArrayList<Country>();
		for(int i = 0; i < array.length(); i++){
			country = gson.fromJson(array.get(i).toString(), Country.class);
			countryList.add(country);
		}
		return countryList;
	}
	
	public static List<City> initCities(Country country) {
		String cityJson = getJson("http://api.vk.com/method/database.getCities?v=5.5&need_all=1&country_id=1");
		JSONObject jsonObj = new JSONObject(cityJson);
		Integer counter = jsonObj.getJSONObject("response").getInt("count");
		List<City> totalCityList = new ArrayList<City>();
		for(int j = 0; j < counter;){
			
			Gson gson = new Gson();
			City city;
			List<City> cityList = new ArrayList<City>();
			cityJson = getJson("http://api.vk.com/method/database.getCities?v=5.5&need_all=1&country_id=1&count=100&offset="+j);
			jsonObj = new JSONObject(cityJson);
			JSONArray array = jsonObj.getJSONObject("response").getJSONArray("items"); 
			for(int i = 0; i < array.length(); i++){
				city = gson.fromJson(array.get(i).toString(), City.class);
				if(!city.getTitle().isEmpty())
					city.setCountry(country);
					cityList.add(city);
			}
			totalCityList.addAll(cityList);
			j+=cityList.size();
		}
		return totalCityList;
	}

	private static String getJson(String apiUrl) {
		StringBuilder str = new StringBuilder("");
		try {

			URL url = new URL(apiUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream()), "UTF-8"));

			String output;
			while ((output = br.readLine()) != null) {
				str.append(output);
			}
			conn.disconnect();
			return str.toString();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return str.toString();
	}
}
