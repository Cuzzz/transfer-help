package com.transfer.kernel.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="message")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	private String title;
	private String body;
	private int status;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="author_id") 
	@NotNull
	private Person author;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="destination_id") 
	@NotNull
	private Person destination;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="upper_id") 
	private Message upperMessage;
	private boolean deletedForAuthor;
	private boolean deletedForDestination;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Person getAuthor() {
		return author;
	}
	public void setAuthor(Person author) {
		this.author = author;
	}
	public Person getDestination() {
		return destination;
	}
	public void setDestination(Person destination) {
		this.destination = destination;
	}
	public Message getUpperMessage() {
		return upperMessage;
	}
	public void setUpperMessage(Message upperMessage) {
		this.upperMessage = upperMessage;
	}
	public boolean isDeletedForAuthor() {
		return deletedForAuthor;
	}
	public void setDeletedForAuthor(boolean deletedForAuthor) {
		this.deletedForAuthor = deletedForAuthor;
	}
	public boolean isDeletedForDestination() {
		return deletedForDestination;
	}
	public void setDeletedForDestination(boolean deletedForDestination) {
		this.deletedForDestination = deletedForDestination;
	}
	
}
