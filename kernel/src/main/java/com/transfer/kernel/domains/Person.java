package com.transfer.kernel.domains;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.transfer.kernel.security.User;

@Entity
@Table(name="person")
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "fd")
	private Timestamp fd;
	
	@Column(name = "td")
	private Timestamp td;
	
	private String gender; 
	
	private String firstname;
	
	private String surname;
	
	private Date birthDate;
	
	private String phoneNumber;
	
	private String avatarUrl;
	
	private String about;
	
	@Column(name = "general_rate")
	private float generalRating;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "role_code")
	private byte roleCode;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id") 
	@NotNull
	private User user;
	
	private int counter;
	
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getFd() {
		return fd;
	}

	public void setFd(Timestamp fd) {
		this.fd = fd;
	}

	public Timestamp getTd() {
		return td;
	}

	public void setTd(Timestamp td) {
		this.td = td;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}
	
	public float getGeneralRating() {
		return generalRating;
	}

	public void setGeneralRating(float generalRating) {
		this.generalRating = generalRating;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public byte getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(byte roleCode) {
		this.roleCode = roleCode;
	}
}
