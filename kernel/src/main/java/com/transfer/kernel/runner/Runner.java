package com.transfer.kernel.runner;

import java.util.Timer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.transfer.kernel.security.User;
import com.transfer.kernel.security.UserRepository;
import com.transfer.kernel.security.UserRole;



@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties
@ComponentScan(basePackages={"com.transfer.kernel.*"})
@EnableJpaRepositories(basePackages = {"com.transfer.kernel.security"
									  ,"com.transfer.kernel.dao"})
public class Runner extends SpringBootServletInitializer
{
    public static void main( String[] args )
    {
    	
    	ConfigurableApplicationContext ctx = SpringApplication.run(Runner.class, args);
    	//checkLocations(ctx);
    	//addUser("admin", "admin", repo);
    	//addUser("user", "user", repo);
    }
    
	static private void addUser(String username, String password, UserRepository userRepository) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(new BCryptPasswordEncoder().encode(password));
		user.grantRole(username.equals("admin") ? UserRole.ADMIN : UserRole.USER);
		userRepository.save(user);
	}
	
/*	static public void checkLocations(ApplicationContext ctx){
		System.out.println("Start checking locations");
    	CountryRepository countryRepo = ctx.getBean(CountryRepository.class);
    	if(countryRepo.count() == 0)
    		countryRepo.save(CityInit.initCountries());
    	Country russia = countryRepo.findOne(1L);
    	CityRepository cityRepo = ctx.getBean(CityRepository.class);
    	if(cityRepo.count() == 0)
    		cityRepo.save(CityInit.initCities(russia));
    	System.out.println("Finish checking locations");
	}*/
	
}
